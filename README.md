# When did I...

A web app for tracking when you did something. Useful for remembering
when you last cleaned your ceiling fans or changed your oil or
whatever you want to remember.

## Local development

First, create a virtual environment, activate it, and install dependencies:

```
$ python3 -m venv .venv
$ . .venv/bin/activate
(.venv) $ pip install --upgrade pip wheel setuptools
(.venv) $ pip install -r requirements.txt -c constraints.txt
```

Then, you can start a development server:

```
(.venv) $ python -m wdi migrate && python -m wdi server
```

Or run the tests:

```
(.venv) $ python -m pytest tests
```

## Deployment

Note that these docs assume you want to store your config and database
directly in your home directory which isn't necessarily the best place
for them.

To deploy, you must install the package which you can do via `pip`
(for example: `pip3 install --user
git+https://gitlab.com/pdebelak/wdi.git`). Then, you can use the `wdi`
command to generate a config file:

```
$ ~/.local/bin/wdi generate-config ~/wdi.sqlite America/Chicago >~/wdi.cfg
```

Next, you can run migrations:

```
$ ~/.local/bin/wdi --config-file ~/wdi.cfg migrate
```

You can create a user:

```
$ ~/.local/bin/wdi --config-file ~/wdi.cfg create-user myusername mypassword
```

Then you can start the server:

```
$ ~/.local/bin/wdi --config-file ~/wdi.cfg server --production
```

Take a look at the help for that command for things like setting a
`--proxy-count` if you are behind a reverse proxy or setting `--host`
or `--port` options, if appropriate.

For dependencies, `wdi` is tested with the packages available from
`apt` on Ubuntu 22.04 at the moment. The `pip` install dependency
versions aren't pinned, so look at the `constraints.txt` file if you
want to know what versions it has been tested with.
