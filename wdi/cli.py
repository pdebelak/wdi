# Copyright 2024 Peter Debelak
#
# This file is part of wdi.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see
# <https://www.gnu.org/licenses/>.
import os
import secrets
import sys
from configparser import ConfigParser
from zoneinfo import ZoneInfo, ZoneInfoNotFoundError

import click
from gunicorn.app.base import BaseApplication
from werkzeug.middleware.proxy_fix import ProxyFix

from wdi import __version__, create_app
from wdi.models import User, run_migrations


class GunicornApplication(BaseApplication):
    def __init__(self, application, options):
        self.application = application
        self.options = options
        super().__init__()

    def load_config(self):
        for key, value in self.options.items():
            self.cfg.set(key, value)

    def load(self):
        return self.application


@click.group()
@click.version_option(__version__)
@click.option("--config-file", default=None, help="The configuration file")
@click.pass_context
def cli(ctx, config_file):
    os.environ["FLASK_APP"] = "wdi"
    ctx.obj = {"app": create_app(config_file)}


@cli.command()
@click.pass_context
def migrate(ctx):
    with ctx.obj["app"].app_context():
        run_migrations()


@cli.command()
@click.argument("db-location")
@click.argument("timezone")
@click.option(
    "--secret-key", default=None, help="The secret key to use for the webserver"
)
def generate_config(db_location, timezone, secret_key):
    if secret_key is None:
        secret_key = secrets.token_hex()
    config = ConfigParser()
    config.add_section("wdi")
    config["wdi"]["db_location"] = db_location
    config["wdi"]["secret_key"] = secret_key
    try:
        ZoneInfo(timezone)
    except ZoneInfoNotFoundError as e:
        raise RuntimeError(f"Could not find timezone {timezone}") from e
    config["wdi"]["timezone"] = timezone
    config.write(sys.stdout)


@cli.command()
@click.argument("username")
@click.argument("password")
@click.pass_context
def create_user(ctx, username, password):
    with ctx.obj["app"].app_context():
        user = User.from_plaintext_password(username, password)
        id = user.save()
        click.echo(f"Created user with id {id}")


@cli.command()
@click.option(
    "--production",
    default=False,
    is_flag=True,
    help="Run in production mode with gunicorn.",
)
@click.option(
    "--port",
    default="5000",
    help="Port to bind to.",
)
@click.option(
    "--host",
    default="127.0.0.1",
    help="Host to bind to.",
)
@click.option(
    "--proxy-count",
    default=0,
    type=int,
    help="Number of trusted proxies flask is deployed behind.",
)
@click.pass_context
def server(ctx, production, port, host, proxy_count):
    if proxy_count > 0:
        ctx.obj["app"].wsgi_app = ProxyFix(
            ctx.obj["app"].wsgi_app,
            x_for=proxy_count,
            x_proto=proxy_count,
            x_host=proxy_count,
            x_prefix=proxy_count,
        )
    if production:
        import multiprocessing

        options = {
            "bind": f"{host}:{port}",
            "workers": (multiprocessing.cpu_count() * 2) + 1,
            "errorlog": "-",
            "accesslog": "-",
        }
        GunicornApplication(ctx.obj["app"], options).run()
    else:
        os.environ["FLASK_ENV"] = "development"
        ctx.obj["app"].run(debug=True, port=port)
