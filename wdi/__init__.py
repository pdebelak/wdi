# Copyright 2024 Peter Debelak
#
# This file is part of wdi.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see
# <https://www.gnu.org/licenses/>.
from typing import Optional

from flask import Flask, g
from flask_login import LoginManager
from flask_wtf.csrf import CSRFProtect

from wdi.config import config
from wdi.models import NotFound, User
from wdi.views import configure_views

__version__ = "0.2.1"


def create_app(config_file: Optional[str] = None):
    config.update_from_file(config_file)
    app = Flask("wdi")
    app.secret_key = config.secret_key
    login_manager = LoginManager(app)

    csrf = CSRFProtect(app)

    configure_views(app, login_manager)

    @login_manager.user_loader
    def load_user(user_id):
        try:
            return User.get(user_id)
        except NotFound:
            return None

    @app.teardown_appcontext
    def teardown_db(exception):
        conn = g.pop("_database", None)

        if conn is not None:
            conn.close()

    return app
