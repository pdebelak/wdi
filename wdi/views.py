# Copyright 2024 Peter Debelak
#
# This file is part of wdi.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see
# <https://www.gnu.org/licenses/>.
from zoneinfo import ZoneInfo

from flask import flash, redirect, render_template, url_for
from flask_login import current_user, login_required, login_user, logout_user

from wdi.config import config
from wdi.forms import CompletionForm, LoginForm, TaskForm
from wdi.models import Completion, NotFound, Task, User


def not_found_error(e):
    return render_template("404.html"), 404


@login_required
def index():
    return render_template("index.html", tasks=Task.for_user(current_user))


def login():
    form = LoginForm()
    if form.validate_on_submit():
        try:
            user = User.from_username(form.username.data)
            if user.password_match(form.password.data):
                login_user(user, remember=True)
                flash("Logged in successfully.")
                return redirect(url_for("index"))
        except NotFound:
            pass
        flash("Login failed due to incorrect username or password")
        return render_template("login.html", form=form)
    return render_template("login.html", form=form)


@login_required
def logout():
    logout_user()
    flash("Logged out successfully.")
    return redirect(url_for("login"))


@login_required
def create_task():
    form = TaskForm()
    if form.validate_on_submit():
        task = Task(description=form.description.data, user_id=current_user.id)
        task.save()
        return redirect(url_for("index"))
    return render_template("new_task.html", form=form)


@login_required
def task(task_id):
    task = Task.get(task_id, current_user)
    form = TaskForm(description=task.description)
    if form.validate_on_submit():
        task.description = form.description.data
        task.save()
        flash("Task updated")
        return redirect(url_for("index"))
    completions = Completion.for_task(task)
    return render_template("task.html", task=task, form=form, completions=completions)


@login_required
def complete_task(task_id):
    task = Task.get(task_id, current_user)
    task.complete()
    return redirect(url_for("index"))


@login_required
def delete_task(task_id):
    task = Task.get(task_id, current_user)
    task.delete()
    flash("Task deleted")
    return redirect(url_for("index"))


@login_required
def completion(completion_id):
    completion = Completion.get(completion_id, current_user)
    form = CompletionForm(completed_at=completion.completed_at)
    if form.validate_on_submit():
        completion.completed_at = form.completed_at.data.replace(
            tzinfo=ZoneInfo(config.timezone)
        )
        completion.save()
        flash("Completed time updated")
        return redirect(url_for("task", task_id=completion.task_id))
    return render_template("completion.html", form=form, completion=completion)


@login_required
def delete_completion(completion_id):
    completion = Completion.get(completion_id, current_user)
    completion.delete()
    flash("Completed time removed")
    return redirect(url_for("task", task_id=completion.task_id))


def configure_views(app, login_manager):
    login_manager.login_view = "login"
    app.errorhandler(404)(not_found_error)
    app.errorhandler(NotFound)(not_found_error)
    app.route("/", methods=["GET"])(index)
    app.route("/login", methods=["GET", "POST"])(login)
    app.route("/logout", methods=["POST"])(logout)
    app.route("/tasks", methods=["GET", "POST"])(create_task)
    app.route("/tasks/<task_id>/complete", methods=["POST"])(complete_task)
    app.route("/tasks/<task_id>/delete", methods=["POST"])(delete_task)
    app.route("/tasks/<task_id>", methods=["GET", "POST"])(task)
    app.route("/completions/<completion_id>", methods=["GET", "POST"])(completion)
    app.route("/completions/<completion_id>/delete", methods=["POST"])(
        delete_completion
    )
