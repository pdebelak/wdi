# Copyright 2024 Peter Debelak
#
# This file is part of wdi.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see
# <https://www.gnu.org/licenses/>.
from configparser import ConfigParser
from pathlib import Path
from typing import Optional


class Config:
    def __init__(self):
        self.db_location = str(Path(__file__).parent.parent / "wdi.sqlite3")
        self.secret_key = "DEVELOPMENT_SECRET_DO_NOT_USE"
        self.timezone = "America/Chicago"

    def update_from_file(self, config_file: Optional[str]):
        if config_file is None:
            return
        parser = ConfigParser()
        parser.read(config_file)
        self.db_location = parser["wdi"]["db_location"]
        self.secret_key = parser["wdi"]["secret_key"]
        self.secret_key = parser["wdi"]["timezone"]


config = Config()
