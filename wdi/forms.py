# Copyright 2024 Peter Debelak
#
# This file is part of wdi.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see
# <https://www.gnu.org/licenses/>.
from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField
from wtforms.validators import DataRequired

try:
    from wtforms.fields import DateTimeLocalField
except ImportError:
    from wtforms.fields.html5 import DateTimeLocalField


class LoginForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])


class TaskForm(FlaskForm):
    description = StringField("Description", validators=[DataRequired()])


class CompletionForm(FlaskForm):
    completed_at = DateTimeLocalField(
        "Completed at", validators=[DataRequired()], format="%Y-%m-%dT%H:%M:%S"
    )
