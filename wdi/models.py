# Copyright 2024 Peter Debelak
#
# This file is part of wdi.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see
# <https://www.gnu.org/licenses/>.
import sqlite3
from datetime import datetime
from typing import List, Optional
from zoneinfo import ZoneInfo

import humanize
from flask import g
from werkzeug.local import LocalProxy
from werkzeug.security import check_password_hash, generate_password_hash

from wdi.config import config


def get_db():
    conn = getattr(g, "_database", None)
    if conn is None:
        conn = g._database = sqlite3.connect(
            config.db_location,
            detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES,
        )
    return conn


db = LocalProxy(get_db)


class NotFound(Exception):
    pass


class User:
    def __init__(self, username: str, password: str, id: Optional[int] = None):
        self.username = username
        self.password = password
        self.id = id
        self.is_authenticated = True
        self.is_active = True
        self.is_anonymous = False

    def get_id(self) -> str:
        if id is None:
            raise Exception("Can't get id for unsaved user")
        return str(self.id)

    @classmethod
    def get(cls, id: int) -> "User":
        res = db.execute("SELECT id, username, password FROM users WHERE id = ?", (id,))
        user = res.fetchone()
        if user is None:
            raise NotFound
        return cls(id=user[0], username=user[1], password=user[2])

    @classmethod
    def from_username(cls, username: str) -> "User":
        res = db.execute(
            "SELECT id, username, password FROM users WHERE username = ?", (username,)
        )
        user = res.fetchone()
        if user is None:
            raise NotFound
        return cls(id=user[0], username=user[1], password=user[2])

    @classmethod
    def from_plaintext_password(cls, username: str, password: str) -> "User":
        return cls(username=username, password=generate_password_hash(password))

    def save(self) -> int:
        with db:
            if self.id is None:
                cursor = db.cursor()
                cursor.execute(
                    "INSERT INTO users (username, password) VALUES (?, ?)",
                    (self.username, self.password),
                )
                self.id = cursor.lastrowid
            else:
                db.execute(
                    "UPDATE users SET username = ?, password = ? WHERE id = ?",
                    (self.username, self.password, self.id),
                )
        return self.id

    def password_match(self, password) -> bool:
        return check_password_hash(self.password, password)


class Task:
    def __init__(
        self,
        description: str,
        user_id: int,
        id: Optional[int] = None,
        last_completed: Optional[datetime] = None,
    ):
        self.description = description
        self.user_id = user_id
        self.id = id
        self.last_completed = _in_config_timezone(last_completed)

    @property
    def last_completed_ago(self) -> Optional[str]:
        if self.last_completed is None:
            return None
        return humanize.naturaltime(
            self.last_completed, when=datetime.now(tz=ZoneInfo(config.timezone))
        )

    def save(self) -> int:
        with db:
            if self.id is None:
                cursor = db.cursor()
                cursor.execute(
                    "INSERT INTO tasks (description, user_id) VALUES (?, ?)",
                    (self.description, self.user_id),
                )
                self.id = cursor.lastrowid
            else:
                db.execute(
                    "UPDATE tasks SET description = ? WHERE id = ?",
                    (self.description, self.id),
                )
        return self.id

    def complete(self) -> int:
        assert self.id is not None
        with db:
            cursor = db.cursor()
            cursor.execute("INSERT INTO completions (task_id) VALUES (?)", (self.id,))
            return cursor.lastrowid

    def delete(self):
        assert self.id is not None
        with db:
            db.execute("DELETE FROM tasks WHERE id = ?", (self.id,))

    @classmethod
    def get(cls, id: int, user: User) -> "Task":
        res = db.execute(
            """
WITH last_completions AS (
  SELECT task_id, MAX(completed_at) as last_completed
  FROM completions
  GROUP BY task_id
)
SELECT tasks.id, tasks.description, tasks.user_id, last_completions.last_completed as "[timestamp]"
FROM tasks
LEFT OUTER JOIN last_completions ON tasks.id = last_completions.task_id
WHERE id = ? AND user_id = ?
""",
            (id, user.id),
        )
        task = res.fetchone()
        if task is None:
            raise NotFound
        return cls(
            id=task[0], description=task[1], user_id=task[2], last_completed=task[3]
        )

    @classmethod
    def for_user(cls, user: User) -> List["Task"]:
        if user.id is None:
            return []
        res = db.execute(
            """
WITH last_completions AS (
  SELECT task_id, MAX(completed_at) as last_completed
  FROM completions
  GROUP BY task_id
)
SELECT tasks.id, tasks.description, last_completions.last_completed as "[timestamp]"
FROM tasks
LEFT OUTER JOIN last_completions ON tasks.id = last_completions.task_id
WHERE user_id = ?
""",
            (user.id,),
        )
        return [
            cls(id=row[0], description=row[1], user_id=user.id, last_completed=row[2])
            for row in res.fetchall()
        ]


class Completion:
    def __init__(self, id: int, task_id: int, completed_at: datetime):
        self.id = id
        self.task_id = task_id
        self.completed_at = _in_config_timezone(completed_at)

    def save(self) -> int:
        with db:
            db.execute(
                "UPDATE completions SET completed_at = ? WHERE id = ?",
                (
                    self.completed_at.astimezone(ZoneInfo("UTC")).replace(tzinfo=None),
                    self.id,
                ),
            )
        return self.id

    def delete(self):
        with db:
            db.execute("DELETE FROM completions WHERE id = ?", (self.id,))

    @classmethod
    def get(cls, id: int, user: User) -> "Completion":
        res = db.execute(
            """
SELECT c.id, c.task_id, c.completed_at
FROM completions AS c
JOIN tasks AS t ON c.task_id = t.id
WHERE c.id = ?
AND t.user_id = ?
""",
            (id, user.id),
        )
        completion = res.fetchone()
        if completion is None:
            raise NotFound
        return cls(id=completion[0], task_id=completion[1], completed_at=completion[2])

    @classmethod
    def for_task(cls, task: Task) -> List["Completion"]:
        if task.id is None:
            return []
        res = db.execute(
            """
SELECT id, task_id, completed_at
FROM completions
WHERE task_id = ?
ORDER BY completed_at DESC
""",
            (task.id,),
        )
        return [
            cls(id=row[0], task_id=row[1], completed_at=row[2])
            for row in res.fetchall()
        ]


def _in_config_timezone(dt: Optional[datetime]):
    if dt is None:
        return None
    if dt.tzinfo is None:
        dt = dt.replace(tzinfo=ZoneInfo("UTC"))
    return dt.astimezone(ZoneInfo(config.timezone))


migrations = [
    [
        """
CREATE TABLE versions (
  version INTEGER UNIQUE
)""",
        """
CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  username STRING UNIQUE,
  password STRING
);
    """,
    ],
    [
        """
CREATE TABLE tasks (
  id INTEGER PRIMARY KEY,
  description TEXT,
  user_id INTEGER NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE
);
""",
        """
CREATE TABLE completions (
  id INTEGER PRIMARY KEY,
  task_id INTEGER NOT NULL,
  completed_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (task_id) REFERENCES tasks (id) ON DELETE CASCADE
);
""",
    ],
    ["CREATE INDEX completions_completed_at ON completions(completed_at);"],
]


def run_migrations():
    try:
        res = db.execute("SELECT version FROM versions")
        version = res.fetchone()[0]
    except sqlite3.OperationalError:
        version = -1
    for migration_num in range(version + 1, len(migrations)):
        migration = migrations[migration_num]
        with db:
            for stmt in migration:
                db.execute(stmt)
            if migration_num == 0:
                db.execute("INSERT INTO versions VALUES (0)")
            else:
                db.execute("UPDATE versions SET version = ?", (migration_num,))
