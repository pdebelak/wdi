# Copyright 2024 Peter Debelak
#
# This file is part of wdi.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see
# <https://www.gnu.org/licenses/>.
from datetime import datetime
from zoneinfo import ZoneInfo

import pytest
from wdi.models import Completion, NotFound, Task, User


def test_edit_completion(client, logged_in_user):
    task = Task(description="Cool completion", user_id=logged_in_user.id)
    task.save()
    completion_id = task.complete()
    response = client.post(
        f"/completions/{completion_id}",
        data={"completed_at": "2024-01-01T10:00:00"},
        follow_redirects=True,
    )
    assert response.status_code == 200
    completion = Completion.get(completion_id, logged_in_user)
    assert completion.completed_at == datetime(
        2024, 1, 1, 10, tzinfo=ZoneInfo("America/Chicago")
    )


def test_edit_completion_incomplete(client, logged_in_user):
    task = Task(description="Cool completion", user_id=logged_in_user.id)
    task.save()
    completion_id = task.complete()
    current_completed_at = Completion.get(completion_id, logged_in_user).completed_at
    response = client.post(
        f"/completions/{completion_id}",
        data={"completed_at": ""},
        follow_redirects=True,
    )
    assert response.status_code == 200
    completion = Completion.get(completion_id, logged_in_user)
    assert completion.completed_at == current_completed_at


def test_edit_completion_wrong_user(client, logged_in_user):
    other_user = User.from_plaintext_password("otheruser", "mypass")
    task = Task(description="Cool completion", user_id=other_user.save())
    task.save()
    completion_id = task.complete()
    response = client.post(
        f"/completions/{completion_id}",
        data={"description": "Cooler completion"},
        follow_redirects=True,
    )
    assert response.status_code == 404


def test_delete_completion(client, logged_in_user):
    task = Task(description="Cool completion", user_id=logged_in_user.id)
    task.save()
    completion_id = task.complete()
    client.post(
        f"/completions/{completion_id}/delete",
        data={},
        follow_redirects=True,
    )
    with pytest.raises(NotFound):
        Completion.get(completion_id, logged_in_user)


def test_delete_completion_wrong_user(client, logged_in_user):
    other_user = User.from_plaintext_password("otheruser", "mypass")
    task = Task(description="Cool completion", user_id=other_user.save())
    task.save()
    completion_id = task.complete()
    response = client.post(
        f"/completions/{completion_id}/delete",
        data={},
        follow_redirects=True,
    )
    assert response.status_code == 404
