# Copyright 2024 Peter Debelak
#
# This file is part of wdi.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see
# <https://www.gnu.org/licenses/>.
import pytest
from wdi.models import NotFound, Task, User


def test_create_task(client, logged_in_user):
    response = client.post(
        "/tasks",
        data={"description": "My task"},
        follow_redirects=True,
    )
    assert b"My task" in response.data
    tasks = Task.for_user(logged_in_user)
    assert len(tasks) == 1
    assert tasks[0].description == "My task"


def test_create_task_missing_data(client, logged_in_user):
    response = client.post(
        "/tasks",
        data={},
        follow_redirects=True,
    )
    assert b"Create task" in response.data


def test_complete_task(client, logged_in_user):
    task = Task(description="Cool task", user_id=logged_in_user.id)
    task_id = task.save()
    response = client.post(
        f"/tasks/{task_id}/complete",
        data={},
        follow_redirects=True,
    )
    assert b"last completed" in response.data
    assert b"now" in response.data
    task = Task.get(task_id, logged_in_user)
    assert task.last_completed is not None


def test_complete_task_wrong_user(client, logged_in_user):
    other_user = User.from_plaintext_password("otheruser", "mypass")
    task = Task(description="Cool task", user_id=other_user.save())
    task_id = task.save()
    response = client.post(
        f"/tasks/{task_id}/complete",
        data={},
        follow_redirects=True,
    )
    assert response.status_code == 404


def test_edit_task(client, logged_in_user):
    task = Task(description="Cool task", user_id=logged_in_user.id)
    task_id = task.save()
    response = client.post(
        f"/tasks/{task_id}",
        data={"description": "Cooler task"},
        follow_redirects=True,
    )
    assert response.status_code == 200
    task = Task.get(task_id, logged_in_user)
    assert task.description == "Cooler task"


def test_edit_task_incomplete(client, logged_in_user):
    task = Task(description="Cool task", user_id=logged_in_user.id)
    task_id = task.save()
    response = client.post(
        f"/tasks/{task_id}",
        data={"description": ""},
        follow_redirects=True,
    )
    assert response.status_code == 200
    task = Task.get(task_id, logged_in_user)
    assert task.description == "Cool task"


def test_edit_task_wrong_user(client, logged_in_user):
    other_user = User.from_plaintext_password("otheruser", "mypass")
    task = Task(description="Cool task", user_id=other_user.save())
    task_id = task.save()
    response = client.post(
        f"/tasks/{task_id}",
        data={"description": "Cooler task"},
        follow_redirects=True,
    )
    assert response.status_code == 404


def test_delete_task(client, logged_in_user):
    task = Task(description="Cool task", user_id=logged_in_user.id)
    task_id = task.save()
    task.complete()  # ensure no foreign key problems
    client.post(
        f"/tasks/{task_id}/delete",
        data={},
        follow_redirects=True,
    )
    with pytest.raises(NotFound):
        Task.get(task_id, logged_in_user)


def test_delete_task_wrong_user(client, logged_in_user):
    other_user = User.from_plaintext_password("otheruser", "mypass")
    task = Task(description="Cool task", user_id=other_user.save())
    task_id = task.save()
    response = client.post(
        f"/tasks/{task_id}/delete",
        data={},
        follow_redirects=True,
    )
    assert response.status_code == 404
