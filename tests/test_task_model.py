# Copyright 2024 Peter Debelak
#
# This file is part of wdi.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see
# <https://www.gnu.org/licenses/>.
from datetime import datetime, timedelta
from zoneinfo import ZoneInfo

from wdi.models import Task


def test_last_completed_empty():
    task = Task(description="my desc", user_id=1)
    assert task.last_completed is None


def test_last_completed_non_timezone_aware():
    task = Task(
        description="my desc", user_id=1, last_completed=datetime(2024, 1, 15, 12)
    )
    assert task.last_completed == datetime(
        2024, 1, 15, 6, tzinfo=ZoneInfo("America/Chicago")
    )


def test_last_completed_timezone_aware():
    task = Task(
        description="my desc",
        user_id=1,
        last_completed=datetime(2024, 1, 15, 12, tzinfo=ZoneInfo("America/New_York")),
    )
    assert task.last_completed == datetime(
        2024, 1, 15, 11, tzinfo=ZoneInfo("America/Chicago")
    )


def test_last_completed_ago_empty():
    task = Task(description="my desc", user_id=1)
    assert task.last_completed_ago is None


def test_last_completed_ago_now():
    task = Task(description="my desc", user_id=1, last_completed=datetime.utcnow())
    assert task.last_completed_ago == "now"


def test_last_completed_ago_past():
    task = Task(
        description="my desc",
        user_id=1,
        last_completed=datetime.utcnow() - timedelta(hours=5),
    )
    assert task.last_completed_ago == "5 hours ago"
