# Copyright 2024 Peter Debelak
#
# This file is part of wdi.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see
# <https://www.gnu.org/licenses/>.
from pathlib import Path

import pytest
from wdi import create_app
from wdi.models import User, run_migrations


@pytest.fixture()
def app():
    app = create_app(str(Path(__file__).parent / "test.cfg"))
    app.config.update({"TESTING": True, "WTF_CSRF_ENABLED": False})
    with app.app_context():
        run_migrations()
        yield app


@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture()
def logged_in_user(client):
    user = User.from_plaintext_password("myuser", "mypass")
    user.save()
    client.post(
        "/login",
        data={"username": "myuser", "password": "mypass"},
    )
    return user
