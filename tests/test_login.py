# Copyright 2024 Peter Debelak
#
# This file is part of wdi.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see
# <https://www.gnu.org/licenses/>.
from wdi.models import User


def test_login_valid_username_password(client):
    user = User.from_plaintext_password("myuser", "mypass")
    user.save()
    response = client.post(
        "/login",
        data={"username": "myuser", "password": "mypass"},
        follow_redirects=True,
    )
    assert len(response.history) == 1
    assert response.request.path == "/"
    assert b"Logged in successfully" in response.data


def test_login_invalid_username(client):
    response = client.post(
        "/login", data={"username": "fake", "password": "mypass"}, follow_redirects=True
    )
    assert b"Login failed" in response.data


def test_login_invalid_password(client):
    user = User.from_plaintext_password("myuser", "mypass")
    user.save()
    response = client.post(
        "/login",
        data={"username": "myuser", "password": "mypass1"},
        follow_redirects=True,
    )
    assert b"Login failed" in response.data
